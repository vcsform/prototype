#ifndef FORMLOADER_H
#define FORMLOADER_H

#include <QObject>
#include <QQmlApplicationEngine>

class FormLoader : public QObject
{
    Q_OBJECT
private:
    QQmlApplicationEngine engine;
public:
    explicit FormLoader(QObject *parent = nullptr);
    void load(QString directory);

signals:

public slots:
    void exit(int retCode);
    void quit();
};

#endif // FORMLOADER_H
