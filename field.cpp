#include "field.h"

Field::Field(QObject *parent)
    : QObject(parent)

{
}

QString Field::name() const
{
    return m_name;
}

void Field::setName(const QString &n)
{
    m_name = n;
}

QVariant Field::value() const
{
    return m_value;
}

void Field::setValue(const QVariant &v)
{
    m_value = v;
}

QDebug operator<<(QDebug debug, const Field &field)
{
    QDebugStateSaver saver(debug);
    debug.nospace() << "Field(" << field.name() << ": " << field.value() << ")";

    return debug;
}
