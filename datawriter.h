#ifndef DATAWRITER_H
#define DATAWRITER_H

#include "persistent.h"

#include <QDir>
#include <QObject>

class DataWriter : public QObject
{
    Q_OBJECT
public:
    explicit DataWriter(const QString &dir, Persistent &persistent, const QString &schemaHash, QObject *parent = nullptr);
    void writeFile();
    void gitCommit();
signals:

private:
    const QString &m_dir;
    const QString &m_schemaHash;
    QDir dataDir;
    Persistent &m_persistent;
};

#endif // DATAWRITER_H
