#include <QDebug>
#include "formloader.h"

FormLoader::FormLoader(QObject *parent) : QObject(parent)
{
    connect(&engine, &QQmlEngine::quit, this, &FormLoader::quit);
    connect(&engine, &QQmlEngine::exit, this, &FormLoader::exit);
}

void FormLoader::load(QString directory)
{
    engine.load(directory + "/schema/main.qml");
    if (engine.rootObjects().isEmpty())
        qWarning() << "Could not load form";

}

void FormLoader::exit(int retCode)
{
    qInfo() << "Formloader shutting down!" << retCode;
}

void FormLoader::quit()
{
    qInfo() << "Formloader quit!";
}
