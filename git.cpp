#include "git.h"
#include <QProcess>

Git::Git(const QString &root, QObject *parent) : QObject(parent), m_root(root)
{
}

bool Git::add(const QString &file)
{
    qInfo() << "git add" << file;

    QProcess p;
    p.setWorkingDirectory(m_root);

    QStringList args = {"add", file};
    p.start("git", args);

    bool result = p.waitForFinished();
    qInfo() << "git added" << file;
    return result;
}

bool Git::commit(const QString &message)
{
    qInfo() << "git commit";

    QProcess p;
    p.setWorkingDirectory(m_root);

    QStringList args = {"commit", "-m", message};
    p.start("git", args);

    bool result = p.waitForFinished();
    qInfo() << "git commited";

    return result;
}

QString Git::hashObject(const QString &path)
{
    qInfo() << "git hash-object";

    QProcess p;
    p.setWorkingDirectory(m_root);

    QStringList args = {"hash-object", path};
    p.start("git", args);

    p.waitForFinished(-1);
    QString result = p.readAllStandardOutput();
    qInfo() << "git hash-object:" << result;

    return result.trimmed();

}
