#include <QDebug>
#include <QQmlContext>
#include <QQmlProperty>
#include <QQmlPropertyMap>
#include <QQmlListProperty>
#include <QQuickItem>
#include "datawriter.h"
#include "mainwindow.h"
#include "persistent.h"
#include "ui_mainwindow.h"
#include "git.h"

MainWindow::MainWindow(const QString &directory, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_directory(directory)
{
    ui->setupUi(this);

    QUrl source("file://" + directory + "/schema/main.qml");
    ui->quickWidget->setSource(source);

    Git git(directory, this);
    schemaHash = git.hashObject("schema/main.qml");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_buttons_accepted()
{
    QQuickItem* rootObject = ui->quickWidget->rootObject();

    Persistent *persistent = rootObject->findChild<Persistent *>();

    qInfo() << persistent->filled();

    DataWriter writer(m_directory, *persistent, schemaHash);

    writer.writeFile();
    writer.gitCommit();

    this->close();
}

