#ifndef FORMLOADER2_H
#define FORMLOADER2_H

#include <QDialog>

namespace Ui {
class FormLoader2;
}

class FormLoader2 : public QDialog
{
    Q_OBJECT

public:
    explicit FormLoader2(QWidget *parent = nullptr);
    ~FormLoader2();

private:
    Ui::FormLoader2 *ui;
};

#endif // FORMLOADER2_H
