#include "datawriter.h"
#include "git.h"
#include <QDebug>
#include <QFile>
#include <QJsonDocument>

DataWriter::DataWriter(const QString &dir, Persistent &persistent, const QString &schemaHash, QObject *parent) :
    QObject(parent),
    m_dir(dir),
    m_persistent(persistent),
    m_schemaHash(schemaHash)
{
    dataDir = QDir(m_dir);
    dataDir.mkdir("data");
    dataDir.cd("data");
    dataDir.mkdir(persistent.uuid());
    dataDir.cd(persistent.uuid());
}

void DataWriter::writeFile()
{
    QString fileName = dataDir.filePath("main.json");

    qInfo() << "Writing to" << fileName;

    QVariantMap form = m_persistent.filled();
    form.insert("_schema", m_schemaHash);
    QJsonDocument doc = QJsonDocument::fromVariant(form);

    QFile file(fileName);
    if(file.open(QIODevice::OpenModeFlag::WriteOnly))
    {
        file.write(doc.toJson(QJsonDocument::Indented));
        file.close();
        qInfo() << "Wrote to" << fileName;
    }
    else
    {
        qInfo() << "Error" << file.errorString();
    }
}

void DataWriter::gitCommit()
{
    Git git(m_dir);
    git.add("data/" + m_persistent.uuid());
    git.commit("Added " + m_persistent.uuid());
}

