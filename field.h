#ifndef FIELD_H
#define FIELD_H

#include <QObject>
#include <QDebug>
#include <QtQml/qqml.h>

class Field : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName)
    Q_PROPERTY(QVariant value READ value WRITE setValue)
    QML_ELEMENT
public:
    Field(QObject *parent = nullptr);

    QString name() const;
    void setName(const QString &);

    QVariant value() const;
    void setValue(const QVariant &);

private:
    QString m_name;
    QVariant m_value;
};

QDebug operator<<(QDebug debug, const Field &field);

#endif // FIELD_H
