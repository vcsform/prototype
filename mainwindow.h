#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(const QString &directory, QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_buttons_accepted();

private:
    Ui::MainWindow *ui;
    QString schemaHash;
    const QString &m_directory;
};

#endif // MAINWINDOW_H
