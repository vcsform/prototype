#ifndef GIT_H
#define GIT_H

#include <QObject>
#include <QDir>

class Git : public QObject
{
    Q_OBJECT
public:
    explicit Git(const QString &root, QObject *parent = nullptr);

    bool add(const QString &file);
    bool commit(const QString &message);
    QString hashObject(const QString &path);
signals:

private:
    const QString &m_root;
};

#endif // GIT_H
