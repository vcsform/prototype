#include "persistent.h"

Persistent::Persistent(QObject *parent)
    : QObject(parent)
{
    m_uuid = QUuid::createUuid().toString(QUuid::WithoutBraces);
}

QQmlListProperty<Field> Persistent::fields()
{
    return {this, this,
            &Persistent::appendField,
            &Persistent::fieldCount,
            &Persistent::field,
            &Persistent::clearFields,
            &Persistent::replaceField,
            &Persistent::removeLastField};
}

void Persistent::appendGuest(Field* p) {
    m_fields.append(p);
}


qsizetype Persistent::fieldCount() const
{
    return m_fields.count();
}

Field *Persistent::field(qsizetype index) const
{
    return m_fields.at(index);
}

void Persistent::clearFields() {
    m_fields.clear();
}

void Persistent::replaceField(qsizetype index, Field *p)
{
    m_fields[index] = p;
}

void Persistent::removeLastField()
{
    m_fields.removeLast();
}

const QString Persistent::uuid()
{
    return m_uuid;
}

QVariantMap Persistent::filled()
{
    QVariantMap form;

    for(qsizetype i = 0; i < fieldCount(); i++) {
        Field *f = this->field(i);
        form.insert(f->name(), f->value());
    }

    return form;
}

void Persistent::appendField(QQmlListProperty<Field>* list, Field* p) {
    reinterpret_cast< Persistent* >(list->data)->appendGuest(p);
}

void Persistent::clearFields(QQmlListProperty<Field>* list) {
    reinterpret_cast< Persistent* >(list->data)->clearFields();
}

void Persistent::replaceField(QQmlListProperty<Field> *list, qsizetype i, Field *p)
{
    reinterpret_cast< Persistent* >(list->data)->replaceField(i, p);
}

void Persistent::removeLastField(QQmlListProperty<Field> *list)
{
    reinterpret_cast< Persistent* >(list->data)->removeLastField();
}

Field* Persistent::field(QQmlListProperty<Field>* list, qsizetype i) {
    return reinterpret_cast< Persistent* >(list->data)->field(i);
}

qsizetype Persistent::fieldCount(QQmlListProperty<Field>* list) {
    return reinterpret_cast< Persistent* >(list->data)->fieldCount();
}
