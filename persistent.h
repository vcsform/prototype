#ifndef PERSISTENT_H
#define PERSISTENT_H

#include <QObject>
#include <QUuid>
#include <QList>
#include <QQmlListProperty>
#include "field.h"

class Persistent : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<Field> fields READ fields)
    Q_CLASSINFO("DefaultProperty", "fields")
    Q_PROPERTY(QString uuid READ uuid CONSTANT)
    QML_ELEMENT
public:
    Persistent(QObject *parent = nullptr);

    QQmlListProperty<Field> fields();
    void appendGuest(Field*);
    qsizetype fieldCount() const;
    Field *field(qsizetype) const;
    void clearFields();
    void replaceField(qsizetype, Field*);
    void removeLastField();

    const QString uuid();

    QVariantMap filled();

private:
    static void appendField(QQmlListProperty<Field>*, Field*);
    static qsizetype fieldCount(QQmlListProperty<Field>*);
    static Field* field(QQmlListProperty<Field>*, qsizetype);
    static void clearFields(QQmlListProperty<Field>*);
    static void replaceField(QQmlListProperty<Field>*, qsizetype, Field*);
    static void removeLastField(QQmlListProperty<Field>*);

    QList<Field *> m_fields;
    QString m_uuid;
};

#endif // PERSISTENT_H
