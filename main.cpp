#include <QCoreApplication>
#include <QDir>
#include <QFile>
#include <QDateTime>
#include <QJsonDocument>
#include <QProcess>
#include <QQmlApplicationEngine>
#include <QApplication>
#include "git.h"
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    // this is the main app
    QApplication app(argc, argv);

    QString dir = "/tmp/qt6-test";

    MainWindow mainwindow(dir);
    mainwindow.show();

    return app.exec();
}


